Alias: v2-0074 = http://terminology.hl7.org/CodeSystem/v2-0074
Alias: ClaimTypeCS = http://terminology.hl7.org/CodeSystem/claim-type
Alias: ProcessPriorityCS = http://terminology.hl7.org/CodeSystem/processpriority
Alias: EHKHinnakiriCS = http://haigekassa.ee/CodeSystem/tervishoiuteenuste-loetelu


Profile:        MedipostDiagnosticReport
Parent:         DiagnosticReport
Id:             medipost-diagnostic-report
Description:    "Medipost analüüside (saatekirja) vastus"          
* category = v2-0074#LAB (exactly)
* identifier 1..* MS
* effective[x] 1..1 MS
* effective[x] only dateTime 
* subject 1..1 MS
* subject only Reference(Patient)
* performer 1..1 MS
* performer only Reference(Practitioner or PractitionerRole or Organization)
* basedOn 0..1 MS 
* basedOn only Reference(ServiceRequest)
* issued 1..1 MS 
* result MS


Instance: InlineServiceRequestPatient1XXX
InstanceOf: ServiceRequest
Usage:  #inline
* id = "xxx"
* identifier[0]
  * system = "https://heda.ph.ee"
  * value = "SR-1924872"
* status = #active 
* intent = #filler-order 
* subject = Reference(Patient/pat1)

Instance: LaboriVastusPatient1Medipost1
InstanceOf: MedipostDiagnosticReport
Usage: #example
* id = "1234567"
* meta.source = "https://heda.ph.ee/diagnosticreports/3546436"
* identifier
  * system = "https://heda.ph.ee"
  * value = "SV-325235"
* status = #final
* category = v2-0074#LAB
* code.coding[0] = SCT#4241000179101 "Laboratory report"
* code.coding[1] = LN#11502-2 "Laboratory report"
* subject = Reference(Patient/pat1)
* effectiveDateTime = "2020-11-24"
* issued = "2023-01-10T18:32:00+03:00"
* performer = Reference(PractitionerRole/PractRole11)
* result[0] = Reference(Observation/BloodGroupObservation)
* result[1] = Reference(Observation/BloodRhObservation)
* result[2] = Reference(Observation/BgPanelObservation)
* result[3] = Reference(Observation/GlucoseObservation)
* contained[0] = InlineServiceRequestPatient1XXX
* basedOn = Reference(InlineServiceRequestPatient1XXX)

Instance: InlineCoveragePatient1
InstanceOf: Coverage
//Usage:  #example
Usage:  #inline
//* id = "53225233232"
* type = http://terminology.hl7.org/CodeSystem/v3-ActCode#PUBLICPOL
* beneficiary = Reference(Patient/pat1)
* payor = Reference(Organization/ins)
* status = #active

Instance: Claim4ServiceRequestPatientXXX
InstanceOf: Claim
Usage: #example
* contained[0] = InlineServiceRequestPatient1XXX
* contained[+] = InlineCoveragePatient1
* status = #active 
* type = ClaimTypeCS#institutional
* use = #claim 
* patient = Reference(Patient/pat1)
* created = "2023-01-10T18:32:00+03:00"
* provider = Reference(Organization/synlab)
* priority = ProcessPriorityCS#normal
* referral = Reference(InlineServiceRequestPatient1XXX)
* supportingInfo
  * sequence = 1
  * category = LN#11502-2 "Laboratory report"
  * valueReference = Reference(DiagnosticReport/1234567)
* insurance
  * sequence = 1
  * focal = true
  * coverage = Reference(InlineCoveragePatient1)
* item[0]
  * sequence = 1
  * productOrService = EHKHinnakiriCS#6025  
  * quantity.value = 2
  * unitPrice.value = 37.30
  * net.value = 74.60
  * net.currency = #EUR
* item[+]
  * sequence = 2
  * productOrService = EHKHinnakiriCS#7033
  * quantity.value = 1
  * unitPrice.value = 13.00
  * net.value = 13.00
  * net.currency = #EUR
