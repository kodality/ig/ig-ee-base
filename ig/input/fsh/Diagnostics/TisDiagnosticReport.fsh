/* This file contains Estonian FHIR standard description*/

Profile:        TisDiagnosticReport
Parent:         DiagnosticReport
Id:             tis-diagnostic-report
Title:          "TIS analüüside ja uuringute (saatekirja) vastus"
Description:    "TEHIKu poolt etteantud profiil labori, diagnostika, radioloogia ja patoloogia vastuste edastamiseks"
* identifier 1..*
* effective[x] only dateTime
* subject 1..1 
* subject only Reference(Patient)
* performer 1..*
* performer only Reference(Practitioner or Organization)

Instance: LaboriVastus1
InstanceOf: TisDiagnosticReport
Usage: #example
* meta.source = "https://heda.ph.ee/diagnosticreports/3546436"
* identifier.system = "https://heda.ph.ee"
* identifier.value = "SV-325235"
* status = #final
* code.coding[0] = SCT#4241000179101 "Laboratory report"
* code.coding[1] = LN#11502-2 "Laboratory report"
* subject = Reference(Patient/pat1)
* effectiveDateTime = "2020-11-24"
* performer[0] = Reference(Organization/Org1)
* performer[1] = Reference(Practitioner/Pract1)
* result[0] = Reference(Observation/BloodGroupObservation)
* result[1] = Reference(Observation/BloodRhObservation)
* result[2] = Reference(Observation/BgPanelObservation)
* result[3] = Reference(Observation/GlucoseObservation)
