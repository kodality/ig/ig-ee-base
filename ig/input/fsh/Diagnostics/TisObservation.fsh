/* This file contains Estonian FHIR standard description*/
Alias: INTERPETATION = http://terminology.hl7.org/CodeSystem/v3-ObservationInterpretation
Alias: OBSCAT = http://terminology.hl7.org/CodeSystem/observation-category
Alias: ABSENTREASON = http://terminology.hl7.org/CodeSystem/data-absent-reason
Alias: QuantityOfMicrobesOID = urn:oid:1.3.6.1.4.1.28284.6.2.1.355.1

Profile:        TisObservation
Parent:         Observation
Id:             tis-observation
Title:          "TIS analüüside ja uuringute tulemuste profiil"
Description:    "TEHIKu poolt etteantud analüüside ja uuringute tulemusi kirjeldatav profiil"
* effective[x] only dateTime
* subject 1..1 
* subject only Reference(Patient)
* performer 1..1
* performer only Reference(Organization or PractitionerRole)
* value[x] only Quantity or string or CodeableConcept or Range
* valueQuantity.system = UCUM

Profile:        TisBloodPressure
Parent:         TisObservation
Id:             tis-blood-pressure
Title:          "TIS vererõhu määramise profiil"
Description:    "TEHIKu poolt etteantud profiil vererõhu mõõtmiste kirjeldamiseks"
* category = OBSCAT#vital-signs "Vital Signs"
* status = #final
* code = LN#85354-9 "Blood pressure panel with all children optional"
* code.text = "Blood pressure systolic and diastolic"
* bodySite 1..1
* component ^slicing.discriminator.type = #pattern
* component ^slicing.discriminator.path = "code"
* component ^slicing.rules = #open
* component ^slicing.description = "Slice based on the component.code pattern"
* component contains systolicBP 1..1 MS and diastolicBP 1..1 MS
* component[systolicBP].code = LN#8480-6 "Systolic blood pressure"
* component[systolicBP].value[x] only Quantity
* component[systolicBP].valueQuantity = UCUM#mm[Hg] "mmHg"
* component[diastolicBP].code = LN#8462-4 "Diastolic blood pressure"
* component[diastolicBP].value[x] only Quantity
* component[diastolicBP].valueQuantity = UCUM#mm[Hg] "mmHg"


Instance: GlucoseObservation
InstanceOf: TisObservation
Usage: #example
* meta.source = "https://heda.ph.ee/observations/214141421420"
* status = #final
* category = OBSCAT#laboratory "Laboratory"
* subject = Reference(Patient/pat1)
* code.coding[0] = LN#15074-8 "Glucose [Moles/volume] in blood"
* effectiveDateTime = "2021-11-24"
* performer = Reference(Organization/Org1)
* valueQuantity.value = 6.3
* valueQuantity = UCUM#mmol/l "mmol/l"
* referenceRange.low.value = 3.1
* referenceRange.low = UCUM#mmol/l "mmol/l"
* referenceRange.high.value = 6.2
* referenceRange.high = UCUM#mmol/l "mmol/l"
* interpretation = INTERPETATION#H "High"


Instance: BloodGroupObservation
InstanceOf: TisObservation
Usage: #example
* meta.source = "https://heda.ph.ee/observations/214141421421"
* category = OBSCAT#laboratory "Laboratory"
* status = #final
* subject = Reference(Patient/pat1)
* code.coding[0] = LN#882-1 "AB0 group [Type] in Blood"
* effectiveDateTime = "2021-11-23"
* performer = Reference(Organization/Org1)
* valueCodeableConcept = SCT#112144000 "Blood group A"
* valueCodeableConcept.text = "A"

Instance: BloodRhObservation
InstanceOf: TisObservation
Usage: #example
* meta.source = "urn:tto:ph:90004527:observation:214141421422"
* category = OBSCAT#laboratory "Laboratory"
* status = #final
* subject = Reference(Patient/pat1)
* code.coding[0] = LN#10331-7 "Rh [Type] in Blood1"
* effectiveDateTime = "2021-11-23"
* performer = Reference(Organization/Org1)
* valueCodeableConcept = SCT#260385009 "Negatiivne"
* valueCodeableConcept.text = "Neg"


Instance: BgPanelObservation
InstanceOf: TisObservation
Usage: #example
* meta.source = "https://heda.ph.ee/observations/214141421423"
* category = OBSCAT#laboratory "Laboratory"
* status = #final
* subject = Reference(Patient/pat1)
* code.coding[0] = LN#34532-2 "Blood type and Indirect antibody screen panel - Blood"
* effectiveDateTime = "2021-11-23"
* performer = Reference(Organization/Org1)
* hasMember[0] = Reference(Observation/BloodGroupObservation)
* hasMember[1] = Reference(Observation/BloodRhObservation)

Instance: BloodPressureObservation
InstanceOf: TisBloodPressure
Usage: #example
* meta.source = "https://heda.ph.ee/observations/214141421424"
* subject = Reference(Patient/pat1)
* effectiveDateTime = "2021-11-23"
* bodySite = SCT#368209003 "Right arm"
* performer = Reference(Organization/Org1)
* component[systolicBP].valueQuantity.value = 120
* component[diastolicBP].valueQuantity.value = 80


Instance: Poc3SkvAnalyysNaatrium
InstanceOf: TisObservation
Usage: #example
* meta.source = "https://heda.ph.ee/observations/214141421425"
* category = OBSCAT#laboratory "Laboratory"
* subject = Reference(Patient/pat1)
* status = #final
* effectiveDateTime = "2020-10-25T09:30:00Z"
* issued = "2020-10-25T09:33:00Z"
* performer = Reference(Organization/Org1)
* specimen = Reference(Specimen/Spcmn1)
* code.coding[0] = LN#2951-2 "Naatrium"
* valueString = "Vabatekstiline vastus"


Instance: Poc3SkvAnalyysHemogramm
InstanceOf: TisObservation
Usage: #example
* meta.source = "https://heda.ph.ee/observations/214141421426"
* category = OBSCAT#laboratory "Laboratory"
* subject = Reference(Patient/pat1)
* status = #final
* effectiveDateTime = "2020-03-11T14:30:00Z"
* issued = "2020-03-11T14:35:00Z"
* performer = Reference(PractitionerRole/PractRole11)
* specimen = Reference(Specimen/Spcmn2)
* code.coding[0] = LN#58410-2 "Hemogramm"
* note[0].text = "Analüüsi tulemuse märkuse info"
* component[0].code.coding[0] = LN#6690-2 "WBC"
* component[0].valueQuantity.value = 7.95
* component[0].valueQuantity = UCUM#10*9/L "10*9/L"
* component[0].referenceRange.low.value = 4.0
* component[0].referenceRange.low = UCUM#10*9/L "10*9/L"
* component[0].referenceRange.high.value = 10.0
* component[0].referenceRange.high = UCUM#10*9/L "10*9/L"
* component[0].interpretation = INTERPETATION#N "Normal"

* component[1].code.coding[0] = LN#789-8 "RBC"
* component[1].valueQuantity.value = 4.4
* component[1].valueQuantity = UCUM#10*12/L "10*12/L"
* component[1].referenceRange.low.value = 4.6
* component[1].referenceRange.low = UCUM#10*12/L "10*12/L"
* component[1].referenceRange.high.value = 6.0
* component[1].referenceRange.high = UCUM#10*12/L "10*12/L"
* component[1].interpretation = INTERPETATION#N "Normal"

* component[2].code.coding[0] = LN#718-7 "Hb"
* component[2].valueQuantity.value = 142
* component[2].valueQuantity = UCUM#g/L "g/L"
* component[2].referenceRange.low.value = 130
* component[2].referenceRange.low = UCUM#g/L "g/L"
* component[2].referenceRange.high.value = 180
* component[2].referenceRange.high = UCUM#g/L "g/L"
* component[2].interpretation.text = "Tulemust tõlgendav tekst"

* component[3].code.coding[0] = LN#51637-7 "Pct"
* component[3].dataAbsentReason.coding[0] = SCT#263703002 "Changed status"
* component[3].dataAbsentReason.coding[1] = ABSENTREASON#not-performed "Not performed"
* component[3].dataAbsentReason.text = "Põhjus, miks Pct tulemus tühistati"


Instance: Poc3SkvAnalyysMikrobioloogia
InstanceOf: TisObservation
Usage: #example
* meta.source = "https://heda.ph.ee/observations/214141421427"
* category = OBSCAT#laboratory "Laboratory"
* subject = Reference(Patient/pat1)
* status = #final
* effectiveDateTime = "2020-03-11T14:30:00Z"
* issued = "2020-03-12T14:32:00Z"
* performer = Reference(Organization/Org1)
* specimen = Reference(Specimen/Spcmn3)
* code.coding[0] = LN#634-6 "Aeroobne külv"
* note[0].text = "Analüüsi tulemuse märkus"
* valueCodeableConcept = SCT#260385009 "negatiivne"
* interpretation.text = "Tulemust tõlgendav tekst"

* component[0].code.coding[0] = LN#45335-7 "Mikroobide samastamine"
* component[0].valueCodeableConcept = SCT#112283007 "Escherichia coli"

* component[1].code.coding[0] = LN#565-2 "Mikroobide hulk külvis"
* component[1].valueCodeableConcept = QuantityOfMicrobesOID#003 "3+"
* component[1].interpretation = INTERPETATION#N "Normal"

* component[2].code.coding[0] = LN#18862-3 "Amoksitsilliin+klavulaanhape"
* component[2].valueCodeableConcept = SCT#30714006 "Resistentne"
* component[2].valueCodeableConcept.text = "Analüüsi tulemuse märkuse info"
* component[2].interpretation.text = "Tulemuse tõlgenduse tekst"
