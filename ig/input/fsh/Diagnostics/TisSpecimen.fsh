/* This file contains Estonian FHIR standard description*/
Alias: BARCODE = urn:oid:1.3.6.1.4.1.28284.1.3.2.12
Alias: ConteinerCondition = http://terminology.hl7.org/ValueSet/v2-0493

Instance: ExampleSpecimen1Seerum
InstanceOf: Specimen
Usage: #example
* id = "Spcmn1"
* identifier.system = BARCODE
* identifier.value = "130315000001-2"
* subject = Reference(Patient/pat1)
* collection.collectedDateTime = "2020-10-25T09:30:00Z"
* collection.bodySite = SCT#181367001 "Veen (vena)"
* container.type = SCT#119364003 "Seerum"

Instance: ExampleSpecimen2Uriin
InstanceOf: Specimen
Usage: #example
* id = "Spcmn2"
* identifier.system = BARCODE
* identifier.value = "130315000001-5"
* subject = Reference(Patient/pat1)
* collection.collectedDateTime = "2020-03-10T10:25:00Z"
* container.type = SCT#258575007 "Uriin, esmane hommikune"

Instance: ExampleSpecimen3Roga
InstanceOf: Specimen
Usage: #example
* id = "Spcmn3"
* identifier.system = BARCODE
* identifier.value = "130315000001-1"
* subject = Reference(Patient/pat1)
* status = #available
* collection.collectedDateTime = "2020-04-02T09:00:00Z"
* collection.bodySite = SCT#119342007 "Sülg"
* container.type = SCT#119334006 "Röga"
* condition[0].text = "Proovimaterjal oli hägune"
