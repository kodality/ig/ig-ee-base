/* This file contains Estonian FHIR standard description*/

Profile:        TisImmunization
Parent:         Immunization
Id:             tis-immunization
Title:          "TIS immuniseerimise profiil"
Description:    "TEHIKu poolt etteantud immuniseerimise profiil"
* vaccineCode from TisVaccineProductValueSet

//// Make this an inline extension (could also be done as a standalone extension)
//* vaccineCode ^slicing.discriminator.type = #value
//* vaccineCode ^slicing.discriminator.path = "coding"
//* vaccineCode ^slicing.rules = #open
//* vaccineCode.coding contains Atc 1..1 MS
//* vaccineCode.coding[Atc].system = $ATC 
//* vaccineCode.coding[Atc].code MS

* occurrence[x] only dateTime
* performer 1..2 
* performer.actor only Reference(Practitioner or Organization)
* performer[Organization].actor 1..1 
* route 0..0
* doseQuantity 1..1 
* doseQuantity.value 1..1 
* doseQuantity = UCUM#ml
* lotNumber 1..1
* isSubpotent 0..0
* subpotentReason 0..0
* programEligibility 0..0
* fundingSource 0..0
* reaction 0..0
* education 0..0
* protocolApplied 0..1
* protocolApplied.targetDisease 1.. 
* protocolApplied.targetDisease from TisImmunizationTargetDiseaseValueSet 
* protocolApplied.doseNumber[x] only positiveInt
* protocolApplied.seriesDoses[x] only positiveInt
* protocolApplied.extension contains RevaccionationDate named revaccionationDate 0..1



Instance: NewImmunization
InstanceOf: TisImmunization
Usage: #example
* meta.source = "https://heda.ph.ee/immunization/214141421421"
* status = #completed
* patient = Reference(Patient/pat1)
* occurrenceDateTime = "2020-11-06"
* vaccineCode.coding[0] = SCT#871826000 "Diphtheria and tetanus vaccine"
//* vaccineCode.coding[1] = $ATC#J07AN01 "tuberkuloosi tekitaja, elus, nõrgestatud"
* lotNumber = #lot1234
* doseQuantity = 0.5 'ml'
* performer[0].function = $IMFUNC#AP
* performer[=].actor = Reference(Organization/Org1)
* performer[+].function = $IMFUNC#AP
* performer[=].actor = Reference(Practitioner/Pract1)
* protocolApplied[0].targetDisease = $TargetDisease#109 "Teetanus"
* protocolApplied[=].doseNumberPositiveInt = 2
//* protocolApplied.extension[revaccinationDate].valueDate = "2021-10-21"

