/* This file contains FHIR to CDA transformations of Immunization resource. It match to ImmuniseerimiseTeatis ja immuniseerimise sektsioon TSK*/

Profile:        TisFHIR2CDAImmunization
Parent:         Immunization
Id:             tis-fhir2cda-immunization
Title:          "TIS CDA-le vastav iimmuniseerimise profiil"
Description:    "TIS CDA sõnumile kohendatud FHIR Immuniseerimise profiil"
* vaccineCode from TisATCValueSet
* doseQuantity 1..1 
* doseQuantity.value 1..1 
* doseQuantity = UCUM#ml
* lotNumber 1..1
* occurrence[x] only dateTime
* performer.actor only Reference(Practitioner or Organization)
* protocolApplied 0..
* protocolApplied.targetDisease 1.. 
* protocolApplied.targetDisease from TisImmunizationTargetDiseaseValueSet 
* protocolApplied.doseNumber[x] only positiveInt
* protocolApplied.seriesDoses[x] only positiveInt
* protocolApplied.extension contains RevaccionationDate named revaccionationDate 0..1
* route 0..0
* isSubpotent 0..0
* subpotentReason 0..0
* programEligibility 0..0
* fundingSource 0..0
* reasonCode 0..0
* education 0..0


Instance: ImmuniseerimiseTeatis
InstanceOf: TisFHIR2CDAImmunization
Usage: #example
* status = #completed
* patient = Reference(Patient/pat1)
* occurrenceDateTime = "2020-11-06"
* vaccineCode = $ATC#J07AM51 "teetanuse toksoid + difteeria toksoid"
* vaccineCode.text = #"D.T. VAX"
* lotNumber = #lot1234
* doseQuantity = 0.5 'ml'
* performer[0].function = $IMFUNC#AP
* performer[0].actor = Reference(Organization/Org1)
* performer[1].function = $IMFUNC#AP
* performer[1].actor = Reference(Practitioner/Pract1)
* protocolApplied.targetDisease[0] = $TargetDisease#102 "Difteeria"
* protocolApplied.targetDisease[1] = $TargetDisease#109 "Teetanus"
* protocolApplied.doseNumberPositiveInt = 2
* protocolApplied.extension[revaccinationDate].valueDate = "2031-10-21"



Instance: ImmuniseerimisedTSKs
InstanceOf: TisFHIR2CDAImmunization
Usage: #example
* status = #completed
* patient = Reference(Patient/pat1)
* occurrenceDateTime = "2020-11-06"
* vaccineCode = $ATC#J07AN01 "tuberkuloosi tekitaja, elus, nõrgestatud"
* vaccineCode.text = #"BCG VACCINE SSI"
* lotNumber = #lot1234
* doseQuantity = 0.5 'ml'
* protocolApplied[0].targetDisease = $TargetDisease#110 "Tuberkuloos"
* protocolApplied[0].doseNumberPositiveInt = 2
* protocolApplied[0].extension[revaccinationDate].valueDate = "2021-10-21"
* protocolApplied[1].targetDisease = $TargetDisease#101	"B-viirushepatiit"
* protocolApplied[1].doseNumberPositiveInt = 4
* protocolApplied[1].extension[revaccinationDate].valueDate = "2022-11-22"


Mapping:  TisImmunizationFHIR2CDA
Source:   TisFHIR2CDAImmunization
Target:   "http://pub.e-tervis.ee/standards2/Standards/8.0"
Title:    "Immunization transformation from FHIR to CDA"
Id:       fhir2cda-mapping-tis-immunization
* -> "ClinicalDocument"
* identifier -> "ClinicalDocument/id"
* identifier.system -> "ClinicalDocument/id[@root]"
* identifier.value -> "ClinicalDocument/id[@extension]"
* text ->               "./component[typeCode='COMP']/structuredBody/component[typeCode='COMP']/section[classCode='DOCSECT', moodCode='EVN']/text/paragraph/content"
* note ->               "./component[typeCode='COMP']/structuredBody/component[typeCode='COMP']/section[classCode='DOCSECT', moodCode='EVN']/text/paragraph/content"
* occurrenceDateTime -> "./component[typeCode='COMP']/structuredBody/component[typeCode='COMP']/section[classCode='DOCSECT', moodCode='EVN']/entry[typeCode='COMP']/procedure/effectiveTime[@value]"
* status ->             "./component[typeCode='COMP']/structuredBody/component[typeCode='COMP']/section[classCode='DOCSECT', moodCode='EVN']/entry[typeCode='COMP']/procedure/statusCode[@code]"
* vaccineCode ->        "./component[typeCode='COMP']/structuredBody/component[typeCode='COMP']/section[classCode='DOCSECT', moodCode='EVN']/entry[typeCode='COMP']/procedure/methodCode[@code]"
* performer[Practitioner].actor -> "./component[typeCode='COMP']/structuredBody/component[typeCode='COMP']/section[classCode='DOCSECT', moodCode='EVN']/entry[typeCode='COMP']/procedure/performer[typeCode='PRF']/assignedEntity/code"
* performer[Organization].actor -> "./component[typeCode='COMP']/structuredBody/component[typeCode='COMP']/section[classCode='DOCSECT', moodCode='EVN']/entry[typeCode='COMP']/procedure/performer[typeCode='PRF']/assignedEntity/representedOrganization"
* doseQuantity.value -> "./component[typeCode='COMP']/structuredBody/component[typeCode='COMP']/section[classCode='DOCSECT', moodCode='EVN']/entry[typeCode='COMP']/procedure/entryRelationship[typeCode='REFR']/substanceAdministration/doseQuantity[@value]"
* doseQuantity.code -> "./component[typeCode='COMP']/structuredBody/component[typeCode='COMP']/section[classCode='DOCSECT', moodCode='EVN']/entry[typeCode='COMP']/procedure/entryRelationship[typeCode='REFR']/substanceAdministration/doseQuantity[@unit]"
* vaccineCode.text -> "./component[typeCode='COMP']/structuredBody/component[typeCode='COMP']/section[classCode='DOCSECT', moodCode='EVN']/entry[typeCode='COMP']/procedure/entryRelationship[typeCode='REFR']/substanceAdministration/consumable/manufacturedProduct/manufacturedMaterial/name"
* lotNumber -> "./component[typeCode='COMP']/structuredBody/component[typeCode='COMP']/section[classCode='DOCSECT', moodCode='EVN']/entry[typeCode='COMP']/procedure/entryRelationship[typeCode='REFR']/substanceAdministration/consumable/manufacturedProduct/manufacturedMaterial/lotNumberText"
* protocolApplied.targetDisease -> "./component[typeCode='COMP']/structuredBody/component[typeCode='COMP']/section[classCode='DOCSECT', moodCode='EVN']/entry[typeCode='COMP']/procedure/entryRelationship[typeCode='REFR']/procedure/methodCode[@code]"
* protocolApplied.extension[revaccinationDate] -> "./component[typeCode='COMP']/structuredBody/component[typeCode='COMP']/section[classCode='DOCSECT', moodCode='EVN']/entry[typeCode='COMP']/procedure/entryRelationship[typeCode='REFR']/procedure/effectiveTime[@value]"

Mapping:  TisImmunizationFHIR2CDA2
Source:   TisFHIR2CDAImmunization
Target:   "http://pub.e-tervis.ee/standards2/Standards/8.0"
Title:    "Immunization transformation from FHIR to CDA (2)"
Id:       fhir2cda-mapping-tis-immunization2
* -> "ClinicalDocument/component[typeCode='COMP']/structuredBody/component[typeCode='COMP']/section[classCode='DOCSECT', moodCode='EVN']/entry[typeCode='COMP']/procedure"
* occurrenceDateTime -> "./effectiveTime[@value]"
* status ->             "./statusCode[@code]"
* performer[Practitioner].actor -> "./performer[typeCode='PRF']/assignedEntity/code"
* performer[Organization].actor -> "./performer[typeCode='PRF']/assignedEntity/representedOrganization"
* doseQuantity.value -> "./entryRelationship[typeCode='REFR']/substanceAdministration/doseQuantity[@value]"
* doseQuantity.code ->  "./entryRelationship[typeCode='REFR']/substanceAdministration/doseQuantity[@unit]"
* vaccineCode.coding.code ->   "./entryRelationship[typeCode='REFR']/substanceAdministration/consumable/manufacturedProduct/manufacturedMaterial/code[@code]"
* vaccineCode.text ->   "./entryRelationship[typeCode='REFR']/substanceAdministration/consumable/manufacturedProduct/manufacturedMaterial/name"
* lotNumber ->          "./entryRelationship[typeCode='REFR']/substanceAdministration/consumable/manufacturedProduct/manufacturedMaterial/lotNumberText"
* protocolApplied.targetDisease -> "./entryRelationship[typeCode='REFR']/procedure/methodCode[@code]"
* protocolApplied.extension[revaccinationDate] -> "./entryRelationship[typeCode='REFR']/procedure/effectiveTime[@value]"
