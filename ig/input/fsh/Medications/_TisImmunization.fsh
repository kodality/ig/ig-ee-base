Alias: $TargetDisease = urn:oid:1.3.6.1.4.1.28284.6.2.1.278
Alias: $IMFUNC = http://terminology.hl7.org/CodeSystem/v2-0443

Extension:   RevaccionationDate
Id:          revaccinationDate
Title:       "Date of the next revacination"
Description: "Järgmise vaktsineerimise kuupäev."
* value[x] only date

ValueSet:    TisVaccineProductValueSet
Title:       "TIS immuniseerimispreparaatide loend (valueset)"
Id:          tis-vaccine-product
Description: "Loend mis kirjeldab erinevad immuniseerimispreparaadid. Kuuluvad SNOMEDi elemend 78785900 |Vaccine product (product)|"
* codes from system SCT where concept is-a SCT#787859002  "Vaccine product (product)"
//* include codes from system $ATC

ValueSet:    TisATCValueSet
Title:       "TIS ATC value set"
Id:          tis-atc
Description: "Codes describing various categories of medicine. Described at oidref.com/2.16.840.1.113883.6.73"
* codes from system $ATC

ValueSet:    TisImmunizationTargetDiseaseValueSet
Title:       "TIS preventable diseases value set"
Id:          tis-immunization-target-diseases
Description: "Codes describing various diseases preventable through immunization. Vaktsiinvälditavad haigused ja haigustekitajad."
* $TargetDisease#101	"B-viirushepatiit"
* $TargetDisease#102	"difteeria"
* $TargetDisease#103	"hemofiilus-nakkus"
* $TargetDisease#104	"leetrid"
* $TargetDisease#105	"läkaköha"
* $TargetDisease#106	"mumps"
* $TargetDisease#107	"poliomüeliit"
* $TargetDisease#109    "teetanus"
* $TargetDisease#110	"tuberkuloos"

