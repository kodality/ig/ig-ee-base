Alias: SCT = http://snomed.info/sct
Alias: UCUM = http://unitsofmeasure.org
Alias: LN = http://loinc.org
Alias: $ATC = urn:oid:2.16.840.1.113883.6.73
Alias: $SPECIALITY = urn:oid:1.3.6.1.4.1.28284.6.2.1.4.5
Alias: $PractROLE = urn:oid:1.3.6.1.4.1.28284.6.2.2.14.4


Alias: IdentityTypeCS =  http://terminology.hl7.org/CodeSystem/v2-0203
Alias: CountryVS = http://hl7.org/fhir/ValueSet/iso3166-1-2 
Alias: BusinessRegister = https://ariregister.rik.ee
Alias: ORGOID = urn:oid:1.3.6.1.4.1.28284.1

