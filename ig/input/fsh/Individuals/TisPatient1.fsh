Profile: TestPatient1
Parent: Patient
Description: "MPI Patient resource with identifiers sliced by type."

* identifier 1..* MS
* identifier ^slicing.discriminator.path = "type"
* identifier ^slicing.discriminator.type = #value
* identifier ^slicing.rules = #open
* identifier ^short = "Patsiendi identifikaatorid vastavalt http://build.fhir.org/valueset-iso3166-1-3.html"

* identifier contains ni 0..* MS
* identifier[ni] ^short = "Riiklik identifikaator"
* identifier[ni].type 1.. MS
* identifier[ni].type = IdentityTypeCS#NI (exactly)
* identifier[ni].system 1..1 MS
* identifier[ni].system from CountryVS (extensible)
* identifier[ni].system ^short = "Süsteemiväärtus peab algama ISO riigikoodiga „urn:iso:std:iso:3166:” ja lõppema 3-kohalise riigikoodiga."
* identifier[ni].value 1..1 MS


* identifier contains passport 0..* MS
* identifier[passport] ^short = "Pass"
* identifier[passport].type 1.. MS
* identifier[passport].type = IdentityTypeCS#PPN (exactly)
* identifier[passport].system 1..1 MS
* identifier[passport].system from CountryVS (extensible)
* identifier[passport].system ^short = "Süsteemiväärtus peab algama ISO riigikoodiga 'urn:iso:std:iso:3166:' ja lõppema selle riigi 3-kohalise koodiga, kus dokument väljastati."
* identifier[passport].value 1..1 MS
* identifier[passport].value ^short = "Passinumber"
* identifier[passport].period MS
* identifier[passport].assigner MS 
* identifier[passport].assigner ^short = "Dokumendi väljastanud organisatsioon. Saab kasutada nii organisatsiooni lingina kui ka tekstina."

* identifier contains idcard 0..* MS
* identifier[idcard] ^short = "ID-card"
* identifier[idcard].type 1.. MS
* identifier[idcard].type = IdentityTypeCS#CZ (exactly)
* identifier[idcard].system 1..1 MS
* identifier[idcard].system from CountryVS (extensible)
* identifier[idcard].system ^short = "Süsteemiväärtus peab algama ISO riigikoodiga 'urn:iso:std:iso:3166:' ja lõppema selle riigi 3-kohalise koodiga, kus dokument väljastati."
* identifier[idcard].value 1..1 MS
* identifier[idcard].value ^short = "ID-kaardi number"
* identifier[idcard].period MS
* identifier[idcard].assigner MS 
* identifier[idcard].assigner ^short = "Dokumendi väljastanud organisatsioon. Saab kasutada nii organisatsiooni lingina kui ka tekstina."

* identifier contains bct 0..1 MS
* identifier[bct] ^short = "Sünnitunnistus"
* identifier[bct].type 1.. MS
* identifier[bct].type = IdentityTypeCS#BCT (exactly)
* identifier[bct].system 1..1 MS
* identifier[bct].system from CountryVS (extensible)
* identifier[bct].system ^short = "Süsteemiväärtus peab algama ISO riigikoodiga 'urn:iso:std:iso:3166:' ja lõppema selle riigi 3-kohalise koodiga, kus dokument väljastati."
* identifier[bct].value 1..1 MS
* identifier[bct].value ^short = "Sünnitunnistuse number."

* identifier contains mr 0..1 MS
* identifier[mr] ^short = "MPI poolt väljaantud 'Medical Record' number"
* identifier[mr].type 1.. MS
* identifier[mr].type = IdentityTypeCS#MR (exactly)
* identifier[mr].system 1..1 MS
* identifier[mr].system = "https://mpi.tehik.ee" (exactly)
* identifier[mr].system ^short = "MPI $generate-identifier operatsiooni abil genereeritud identifikaator."
* identifier[mr].value 1..1 MS

* identifier contains other 0..1 MS
* identifier[other] ^short = "Muu isikutunnistusena mitte aktsepteeritav dokument. Kasutada ainult erandolukordades!!!! Või saame piirduda sel juhul MRN-iga????"
* identifier[other].type 1.. MS
* identifier[other].type = IdentityTypeCS#U (exactly)
* identifier[other].system 1..1 MS
//* identifier[other].system from "urn:" (extensible)
* identifier[other].system ^short = "MPI $generate-identifier operatsiooni abil genereeritud identifikaator."
* identifier[other].value 1..1 MS

* identifier contains internal 0..* MS
* identifier[internal] ^short = "Asutuse sisene patsiendi identifikaator"
* identifier[internal].type 1.. MS
* identifier[internal].type = IdentityTypeCS#PRN (exactly)
* identifier[internal].system 1..1 MS
//* identifier[internal].system from "urn:oid:ee.rik.ariregister" (extensible)
* identifier[internal].system from ORGOID (extensible)
* identifier[internal].system ^short = "Siin on kaks võimalust panna https://pub.e-tervis.ee/oids.py/viewform?oid=1.3.6.1.4.1.28284.1 valueseti ja valideerida need andmed või panna viitena Äriregistrile (viimane on IB eelistus)."
* identifier[internal].value 1..1 MS


Instance: PatientIgorBossenko
InstanceOf: TestPatient1
Description: "Välismaalase kodeerimine."
Usage: #example
* id = "pat1"
* identifier[0]
  * type = IdentityTypeCS#NI
  * system = "urn:iso:std:iso:3166:EST"
  * value = "37302102711"
* identifier[+]
  * type = IdentityTypeCS#NI
  * system = "urn:iso:std:iso:3166:UZB"
  * value = "31002736540023"
* identifier[+]
  * type = IdentityTypeCS#CZ
  * system = "urn:iso:std:iso:3166:EST"
  * value = "AB0421183"
  * period.end = "2023-12-28"
  * assigner.display = "Estonian Police and Board Agency"
* identifier[+]
  * type = IdentityTypeCS#PPN
  * system = "urn:iso:std:iso:3166:EST"
  * value = "K0307337"
  * period.end = "2023-12-28"
  * assigner.display = "Estonian Police and Board Agency"
* identifier[+]
  * type = IdentityTypeCS#MR
  * system = "https://mpi.tehik.ee"
  * value = "82746127612"
* identifier[+]
  * type = IdentityTypeCS#U
  * system = "https://his.haigla.ee/patients"
  * value = "123e4567-e89b-12d3-a456-426614174000"
* identifier[+]
  * type = IdentityTypeCS#PRN
  * system = "urn:oid:1.3.6.1.4.1.28284.1.3"
  * value = "123e4567-e89b-12d3-a456-426614174000"
* active = true
* name[0]
  * use = #official
  * given = "Igor"
  * family = "Bossenko"
* gender = #male
* birthDate = "1973-02-10"
* address.line = "Valukoja 10"
* address.city = "Tallinn"
* address.country = "EST"
* address.postalCode = "14215"


