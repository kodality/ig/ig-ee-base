
Profile: TestPatient3
Parent: Patient
Description: "MPI Patient resource with identifiers."
* identifier 1..* MS
* identifier.system from PatientIdentityVS (required)
* identifier.system 1..1 MS
* identifier.value 1..1 MS
* identifier obeys mpi-id-01

Instance: PatientIgorBossenko3
InstanceOf: TestPatient3
Usage: #example
* id = "pat3"
* identifier[0]
  * system = "urn:pin:hl7.ee:pid:ni:est"
  * value = "37302102711"
* identifier[+]
  * system = "urn:pin:hl7.ee:pid:ni:uzb"
  * value = "31002736540023"
* identifier[+]
  * system = "urn:pin:hl7.ee:pid:cz:est"
  * value = "AB0421183"
  * period.end = "2023-12-28"
  * assigner.display = "Estonian Police and Board Agency"
* identifier[+]
  * system = "urn:pin:hl7.ee:pid:ppn:est"
  * value = "K0307337"
  * period.end = "2023-12-28"
  * assigner.display = "Estonian Police and Board Agency"
* identifier[+]
//  * type = IdentityTypeCS#MR
  * system = "https://mpi.tehik.ee"
  * value = "82746127612"
* identifier[+]
//  * type = IdentityTypeCS#PRN
//  * system = "urn:oid:1.3.6.1.4.1.28284.1.3"
  * system = "urn:pin:hl7.ee:pid:prn:90006399"
  * value = "123e4567-e89b-12d3-a456-426614174000"
* identifier[+]
// * type = IdentityTypeCS#U
  * system = "urn:pin:hl7.ee:pid:u"
  * value = "90006399:xyz:123e4567-e89b-12d3-a456-426614174000"
* active = true
* name[0]
  * use = #official
  * given = "Igor"
  * family = "Bossenko"
* gender = #male
* birthDate = "1973-02-10"
* address.line = "Valukoja 10"
* address.city = "Tallinn"
* address.country = "EST"
* address.postalCode = "14215"


