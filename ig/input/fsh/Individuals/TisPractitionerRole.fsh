/* This file contains Estonian FHIR standard description*/
// Alias: $EEBase-Practitioner = https://hl7.ee/fhir/StructureDefinition/EEBase-Practitioner
// Alias: $EEBase-Organization = https://hl7.ee/fhir/StructureDefinition/EEBase-Organization
// Alias: $EEBase-Location = https://hl7.ee/fhir/StructureDefinition/EEBase-Location
// Alias: $EEBase-HealthcareService = https://hl7.ee/fhir/StructureDefinition/EEBase-HealthcareService
// Alias: $practitioner-role = http://hl7.ee/fhir/ValueSet/practitioner-role
// Alias: $c80-practice-codes = http://hl7.ee/fhir/ValueSet/c80-practice-codes

CodeSystem: EEOccupationCS
Id:         ee-occupation
Title:     "Eesti ametite klassifikaator"
Description: "Eesti ametite klassifikaator"
* ^url =  http://tis.tehik.ee/terminology/fhir/CodeSystem/ee-occupation
* #22122501 "Pediaater"
* #22121101 "Allergoloog-immunoloog"
* #22120901 "Kardioloog"
* #22210502 "Õde"
* #PN "Advanced Practice Nurse"

ValueSet: EEBaseOccupationVS
Id: ee-base-occupation
Title: "Ametinimetus"
Description: "Ametinimetus"
//* include codes from system SCT where concept descendent-of #14679004 "Occupation"
* include codes from system EEOccupationCS
//* include codes from system http://terminology.hl7.org/CodeSystem/v2-0360 where concept = #PN "Advanced Practice Nurse"

CodeSystem: EEErialaCS
Id:         ee-eriala
Title:     "TIS erialade loend"
Description: "TIS erialade loend"
* ^url =  http://tis.tehik.ee/terminology/fhir/CodeSystem/ee-eriala
* #E170 "Kardioloogia"
* #E290 "Pediaatria"
* #E670 "Pediaatria allergoloogia lisapädevusega"
* #N100 "Intensiivõendus"
* #N200 "Kliiniline õendus"
* #N300 "Terviseõendus"
* #N500 "Õde"

ValueSet: EEBaseErialaVS
Id: ee-base-eriala
Title: "Eriala (kutse)"
Description: "Eriala (kutse)"
* include codes from system EEErialaCS

ValueSet: EEBaseSpecialtyVS
Id: ee-base-specialty
Title: "Tegevusvaldkond"
Description: "Tegevusvaldkond"
* include codes from system SCT where concept descendent-of #394658006 "Clinical specialty"

ValueSet: EEBasePractitionerRoleVS
Id: ee-base-practitioner-role
Title: "Roll"
Description: "Töötaja roll TTO-s"
* include codes from system http://terminology.hl7.org/CodeSystem/practitioner-role


Profile: EEBasePractitionerRole
Parent: PractitionerRole
Id: EEBase-PractitionerRole
Description: "Töösuhe. Tervishoiu töötaja roll tervishoiu asutuses."
Title: "EEBase PractitionerRole"
* ^version = "1.0.0"
* ^status = #draft
* ^publisher = "HL7 Estonia"
* active 1.. MS
* practitioner 1.. MS
* practitioner only Reference(EEBasePractitioner)
* practitioner.reference MS
* organization 1.. MS
//* organization only Reference($EEBase-Organization)
* organization.reference MS
* code 1.. MS
* code ^short = "Tervishoiutöötaja roll TTO-s"
* code ^slicing.discriminator.type = #value
* code ^slicing.discriminator.path = "coding.system"
* code ^slicing.rules = #open
* code contains
    role 1..1 MS and
    tor 0..1 MS
* code[role].coding.system = "http://terminology.hl7.org/CodeSystem/practitioner-role" (exactly)
* code[role] ^short = "Roll TTO-s"
* code[role] from EEBasePractitionerRoleVS (required)
* code[tor].coding.system = "http://tis.tehik.ee/terminology/fhir/CodeSystem/ee-occupation" (exactly)
* code[tor] ^short = "Ametinimetus (Töötamiseregistri andmete järgi)"
* code[tor] from EEBaseOccupationVS (required)

* specialty ^short = "Tegevusvaldkond ja eriala"
* specialty ^slicing.discriminator.type = #value
* specialty ^slicing.discriminator.path = "coding.system"
* specialty ^slicing.rules = #open
* specialty contains
    tegevusvaldkond 0..* MS and
    eriala 0..* MS
* specialty[tegevusvaldkond].coding.system = SCT (exactly)
* specialty[tegevusvaldkond] ^short = "Tegevusvaldkond"
* specialty[tegevusvaldkond] from EEBaseSpecialtyVS (required)
* specialty[eriala].coding.system = "http://tis.tehik.ee/terminology/fhir/CodeSystem/ee-eriala" (exactly)
* specialty[eriala] ^short = "Eriala"
* specialty[eriala] from EEBaseErialaVS (required)
//* location only Reference($EEBase-Location)
//* healthcareService only Reference($EEBase-HealthcareService)

Instance: PractitionerRoleJulgeHuntAtKodality
InstanceOf: EEBasePractitionerRole
Usage: #example
* id = "PractRole11"
* active = true
* period.start = "2012-01-01"
* practitioner = Reference(Practitioner/Pract1)
* organization = Reference(Organization/Org1)
* code[role] = http://terminology.hl7.org/CodeSystem/practitioner-role#doctor "Doctor"


Instance: PractitionerRoleD05647
InstanceOf: EEBasePractitionerRole
Usage: #example
* id = "PractRoleD05647"
* active = true
* period.start = "2012-01-01"
* practitioner = Reference(Practitioner/D05647)
* organization = Reference(Organization/Org1)
* code[role] = http://terminology.hl7.org/CodeSystem/practitioner-role#doctor "Doctor"
* code[tor] = EEOccupationCS#22122501 "Pediaater"
* specialty[0] = SCT#418535003 "Pediatric specialty"
* specialty[+] = SCT#418535003 "Pediatric immunology"
* specialty[+] = SCT#418535003 "Allergy - specialty"
* specialty[+] = EEErialaCS#E290 "Pediaatria"
* specialty[+] = EEErialaCS#E670 "Pediaatria allergoloogia lisapädevusega"


Instance: PractitionerRoleD04281
InstanceOf: EEBasePractitionerRole
Usage: #example
* id = "PractRoleD04281"
* active = true
* period.start = "2012-01-01"
* practitioner = Reference(Practitioner/D04281)
* organization = Reference(Organization/Org1)
* code[role] = http://terminology.hl7.org/CodeSystem/practitioner-role#doctor "Doctor"
* code[tor] = EEOccupationCS#22120901 "Kardioloog"
* specialty[0] = SCT#394579002 "Cardiology"
* specialty[+] = SCT#1251549008 "Interventional cardiology"
* specialty[+] = EEErialaCS#E170 "Kardioloogia"




Instance: PractitionerRoleN12770
InstanceOf: EEBasePractitionerRole
Usage: #example
* id = "PractRoleN12770"
* active = true
* period.start = "2012-01-01"
* practitioner = Reference(Practitioner/N12770)
* organization = Reference(Organization/Org1)
* code[role] = http://terminology.hl7.org/CodeSystem/practitioner-role#nurse "Nurse"
* code[tor] = EEOccupationCS#PN "Advanced Practice Nurse"
* specialty[0] = SCT#394810000 "Rheumatology"
* specialty[+] = EEErialaCS#N200 "Kliiniline õendus"


Instance: PractitionerRoleN13001
InstanceOf: EEBasePractitionerRole
Usage: #example
* id = "PractRoleN13001"
* active = true
* period.start = "2012-01-01"
* practitioner = Reference(Practitioner/N13001)
* organization = Reference(Organization/Org1)
* code[role] = http://terminology.hl7.org/CodeSystem/practitioner-role#nurse "Nurse"
* code[tor] = EEOccupationCS#PN "Advanced Practice Nurse"
* specialty[0] = SCT#773568002 "Emergency medicine"
* specialty[+] = EEErialaCS#N100 "Intensiivõendus"


Instance: PractitionerRoleN00778
InstanceOf: EEBasePractitionerRole
Usage: #example
* id = "PractRoleN00778"
* active = true
* period.start = "2012-01-01"
* practitioner = Reference(Practitioner/N00778)
* organization = Reference(Organization/Org1)
* code[role] = http://terminology.hl7.org/CodeSystem/practitioner-role#nurse "Nurse"
* code[tor] = EEOccupationCS#PN "Advanced Practice Nurse"
* specialty[0] = SCT#394821009 "Occupational medicine"
* specialty[+] = EEErialaCS#N300 "Terviseõendus"


Instance: PractitionerRoleN14105
InstanceOf: EEBasePractitionerRole
Usage: #example
* id = "PractRoleN14105"
* active = true
* period.start = "2012-01-01"
* practitioner = Reference(Practitioner/N14105)
* organization = Reference(Organization/Org1)
* code[role] = http://terminology.hl7.org/CodeSystem/practitioner-role#nurse "Nurse"
* code[tor] = EEOccupationCS#22210502 "Õde"
* specialty[0] = SCT#722165004 "Nursing"
* specialty[0] = SCT#394537008 "Pediatric specialty"
* specialty[+] = EEErialaCS#N500 "Õde"

