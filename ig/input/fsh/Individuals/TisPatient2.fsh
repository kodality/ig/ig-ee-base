Alias: PIDNI = urn:pin:hl7.ee:pid:ni
Alias: PIDPPN = urn:pin:hl7.ee:pid:ppn
Alias: PIDCZ = urn:pin:hl7.ee:pid:cz
Alias: PIDBCT = urn:pin:hl7.ee:pid:bct
Alias: PIDUNK = urn:pin:hl7.ee:pid:u
Alias: PIDORGINT = urn:pin:hl7.ee:pid:prn

Profile: TestPatient2
Parent: Patient
Description: "MPI Patient resource with identifiers."

* identifier 1..* MS
* identifier ^slicing.discriminator.path = "system"
* identifier ^slicing.discriminator.type = #value
* identifier ^slicing.rules = #open
* identifier ^short = "Patsiendi identifikaatorid. Patsiendisüsteemi namespace-iks on , kus urn:pin spetsifitseeritud RFC-s https://www.rfc-editor.org/rfc/rfc3043.html. Ning 'hl7.ee:pid' on patsiendi namespace. Viies element vastab http://terminology.hl7.org/CodeSystem/v2-0203 koodisüsteemi väärtusele. Kuues element tüüpiliselt vastab riigi koodile koodisüsteemist http://hl7.org/fhir/ValueSet/iso3166-1-3 või asutuse registrikoodile."

* identifier contains ni 0..* MS
* identifier[ni] ^short = "Riiklik identifikaator"
* identifier[ni].system from PatientIdentityVS (extensible)
* identifier[ni].system = PIDNI
* identifier[ni].system ^short = "Süsteemiväärtus peab algama urn-iga 'urn:pin:hl7.ee:pid:ni:' ja lõppema 3-kohalise riigikoodiga."
* identifier[ni].value 1..1 MS
* identifier[ni].value ^short = "Isikukood või väisriigi riiklik identifikaator"

* identifier contains passport 0..* MS
* identifier[passport] ^short = "Pass"
* identifier[passport].system 1..1 MS
* identifier[passport].system from PatientIdentityVS (extensible)
* identifier[passport].system = PIDPPN
* identifier[passport].system ^short = "Süsteemiväärtus peab algama urn-iga 'urn:pin:hl7.ee:pid:ppn:' ja lõppema selle riigi 3-kohalise koodiga, kus dokument väljastati."
* identifier[passport].value 1..1 MS
* identifier[passport].value ^short = "Passinumber"
* identifier[passport].period MS
* identifier[passport].assigner MS 
* identifier[passport].assigner ^short = "Dokumendi väljastanud organisatsioon. Saab kasutada nii organisatsiooni lingina kui ka tekstina."

* identifier contains idcard 0..* MS
* identifier[idcard] ^short = "ID-card"
* identifier[idcard].system 1..1 MS
* identifier[idcard].system from PatientIdentityVS (extensible)
* identifier[idcard].system = PIDCZ
* identifier[idcard].system ^short = "Süsteemiväärtus peab algama urn-iga 'urn:pin:hl7.ee:pid:cz:' ja lõppema selle riigi 3-kohalise koodiga, kus dokument väljastati."
* identifier[idcard].value 1..1 MS
* identifier[idcard].value ^short = "ID-kaardi number"
* identifier[idcard].period MS
* identifier[idcard].assigner MS 
* identifier[idcard].assigner ^short = "Dokumendi väljastanud organisatsioon. Saab kasutada nii organisatsiooni lingina kui ka tekstina."

* identifier contains mr 0..1 MS
* identifier[mr] ^short = "MPI poolt väljaantud 'Medical Record' number"
* identifier[mr].system 1..1 MS
* identifier[mr].system = "https://mpi.tehik.ee" (exactly)
* identifier[mr].system ^short = "MPI $generate-identifier operatsiooni abil genereeritud identifikaator."
* identifier[mr].value 1..1 MS

* identifier contains bct 0..1 MS
* identifier[bct] ^short = "Sünnitunnistus"
* identifier[bct].system 1..1 MS
* identifier[bct].system from PatientIdentityVS (extensible)
* identifier[bct].system = PIDBCT
* identifier[bct].system ^short = "Süsteemiväärtus peab algama urn-iga 'urn:pin:hl7.ee:pid:bct:' ja lõppema selle riigi 3-kohalise koodiga, kus dokument väljastati."
* identifier[bct].value 1..1 MS
* identifier[bct].value ^short = "Sünnitunnistuse number."

* identifier contains internal 0..* MS
* identifier[internal] ^short = "Asutuse sisene patsiendi identifikaator"
* identifier[internal].system 1..1 MS
* identifier[internal].system from PatientIdentityVS (extensible)
* identifier[internal].system = PIDORGINT 
* identifier[internal].system ^short = "Süsteemiväärtus peab algama urn-iga 'urn:pin:hl7.ee:pid:prn:' ja lõppema koodi väljastava organisatsiooni registrikoodiga."
* identifier[internal].value 1..1 MS
* identifier[internal].value ^short = "Sisemine identifikaator peab olema unikaalne asutuse sees. Identifikaator ei pea kandma mingit semantilist tähendust."

* identifier contains other 0..1 MS
* identifier[other] ^short = "Muu isikutunnistusena mitte aktsepteeritav dokument. Kasutada ainult erandolukordades!!!!  Iga kasutamist valideeritakse TEHIKu poolt. Või saame piirduda sel juhul MRN-iga????"
* identifier[other].system 1..1 MS
//* identifier[other].system from PatientIdentityVS (extensible)
* identifier[other].system = "urn:pin:hl7.ee:pid:u" (exactly)
* identifier[other].system ^short = "Varuvariant igasuguse prügi panemiseks, kui ükski teistest identifikaatoritest ei sobi. Süsteemiväärtus peab algama urn-iga 'urn:pin:hl7.ee:pid:u:' ja lõppema organisatsiooni registrikoodiga ja unikaalse väärtusega väljastava organisatsiooni sees."
* identifier[other].value 1..1 MS
* identifier[other].value ^short = "Saatja peab tagama väärtuse unikaalsust (maailmaa piires)."



Instance: PatientIgorBossenko2222
InstanceOf: TestPatient2
Description: "Välismaalase kodeerimine."
Usage: #example
* id = "pat222"
* identifier[0]
  * system = "urn:pin:hl7.ee:pid:ni:est"
  * value = "37302102711"
* identifier[+]
  * system = "urn:pin:hl7.ee:pid:ni:uzb"
  * value = "31002736540023"
* identifier[+]
  * system = "urn:pin:hl7.ee:pid:cz:est"
  * value = "AB0421183"
  * period.end = "2023-12-28"
  * assigner.display = "Estonian Police and Board Agency"
* identifier[+]
  * system = "urn:pin:hl7.ee:pid:ppn:est"
  * value = "K0307337"
  * period.end = "2023-12-28"
  * assigner.display = "Estonian Police and Board Agency"
* identifier[+]
//  * type = IdentityTypeCS#MR
  * system = "https://mpi.tehik.ee"
  * value = "82746127612"
* identifier[+]
//  * type = IdentityTypeCS#PRN
//  * system = "urn:oid:1.3.6.1.4.1.28284.1.3"
  * system = "urn:pin:hl7.ee:pid:prn:90006399"
  * value = "123e4567-e89b-12d3-a456-426614174000"
* identifier[+]
// * type = IdentityTypeCS#U
  * system = "urn:pin:hl7.ee:pid:u"
  * value = "90006399:xyz:123e4567-e89b-12d3-a456-426614174000"
* active = true
* name[0]
  * use = #official
  * given = "Igor"
  * family = "Bossenko"
* gender = #male
* birthDate = "1973-02-10"
* address.line = "Valukoja 10"
* address.city = "Tallinn"
* address.country = "EST"
* address.postalCode = "14215"





Profile: MPIPatient2eng
Parent: Patient
Description: "MPI Patient resource with identifiers."

* identifier 1..* MS
* identifier ^slicing.discriminator.path = "system"
* identifier ^slicing.discriminator.type = #value
* identifier ^slicing.rules = #open
* identifier ^short = "Patient identifiers. The patient system namespace starts with urn:pin according to https://www.rfc-editor.org/rfc/rfc3043.html. 'hl7.ee' fixed as issuer and 'pid' fixed as namespace patient identifiers. The fifth element should match to the code system http://terminology.hl7.org/CodeSystem/v2-0203. The sixth element typically specifying the territory (http://hl7.org/fhir/ValueSet/iso3166-1-3) or organization."
//* identifier obeys mpi-id-01

* identifier contains ni 0..* MS
* identifier[ni] ^short = "National identifier"
* identifier[ni].system from PatientIdentityNI (required)
//* identifier[ni].system = PIDNI
* identifier[ni].system ^short = "System should start with 'urn:pin:hl7.ee:pid:ni:' and end with a 3-digit country code."
* identifier[ni].value 1..1 MS
* identifier[ni].value ^short = "Isikukood or national identifier of the foreign country"

* identifier contains passport 0..* MS
* identifier[passport] ^short = "Passport"
* identifier[passport].system 1..1 MS
* identifier[passport].system from PatientIdentityPPN (required)
//* identifier[passport].system = PIDPPN
* identifier[passport].system ^short = "System should start with 'urn:pin:hl7.ee:pid:ppn:' and end with a 3-digit country code, where document issued."
* identifier[passport].value 1..1 MS
* identifier[passport].value ^short = "Passport number"
* identifier[passport].period MS
* identifier[passport].assigner MS 
* identifier[passport].assigner ^short = "The organization that issued the document. Can be used both as reference to organization or as text."

* identifier contains bct 0..1 MS
* identifier[bct] ^short = "Birth certificate"
* identifier[bct].system 1..1 MS
* identifier[bct].system from PatientIdentityBCT (required)
//* identifier[bct].system = PIDBCT
* identifier[bct].system ^short = "System should start with 'urn:pin:hl7.ee:pid:bct:' and end with a 3-digit country code, where document issued."
* identifier[bct].value 1..1 MS
* identifier[bct].value ^short = "Birth certificate number."

