/* This file contains Estonian FHIR standard description*/
//Alias: $ee-medre-pid = https://hl7.ee/NamingSystem/pin-org-est-medre
//Alias: $EEBase-ESTID = https://hl7.ee/NamingSystem/pin-pid-est-ni
//Alias: $EEBase-Address = https://hl7.ee/fhir/StructureDefinition/EEBase-Address
//Alias: $EEBase-Organization = https://hl7.ee/fhir/StructureDefinition/EEBase-Organization

ValueSet: EEBaseQualificationVS
Id: ee-base-qualification
Title: "Kvalifikatsioon (Haridus)"
Description: "Eriala (kvalifikatsioon)"
* include codes from system http://terminology.hl7.org/CodeSystem/v2-0360 where concept is-a #MD "Doctor of Medicine" and concept is-a #PN "Advanced Practice Nurse" and concept is-a #CRN "Certified Registered Nurse" 


Profile: EEBasePractitioner
Parent: Practitioner
Id: EEBase-Practitioner
Description: "Tervishoiu töötaja"
Title: "EEBase Practitioner"
* ^version = "1.0.0"
* ^status = #draft
* ^publisher = "HL7 Estonia"
* identifier 1.. MS
* identifier ^short = "Tervishoiutöötaja identifikaator"
* identifier ^slicing.discriminator.type = #value
* identifier ^slicing.discriminator.path = "system"
* identifier ^slicing.rules = #open
* identifier contains
    medre 0..1 MS and
    est-id 0..1 MS
* identifier[medre].system = "urn:pin:hl7.ee:org:est:prn:70008799" (exactly)
* identifier[medre] ^definition = "MEDRE identifier of healthcare practitioner provided by Terviseamet"
* identifier[medre] ^short = "Riiklikku MEDRE registrisse kantud tervishoiutöötaja kood. Hallatakse Terviseameti poolt."
* identifier[est-id].system = "urn:pin:hl7.ee:pid:est:ni" (exactly)
* identifier[est-id] ^short = "Eesti isikukoodi identifikaator"
* name 1..1 MS
* name.family 1.. MS
* name.given 1..1 MS
//* address only $EEBase-Address
* qualification.code from EEBaseQualificationVS (required)
* qualification.code ^short = "Kvalifikatsioon,haridus, litsents"
//* qualification.issuer only Reference($EEBase-Organization)
* communication from AllLanguages (preferred)
* communication ^binding.extension[0].url = "http://hl7.org/fhir/StructureDefinition/elementdefinition-maxValueSet"
* communication ^binding.extension[=].valueCanonical = "http://hl7.org/fhir/ValueSet/all-languages"
* communication ^binding.extension[+].url = "http://hl7.org/fhir/StructureDefinition/elementdefinition-bindingName"
* communication ^binding.extension[=].valueString = "Language"
* communication ^binding.extension[+].url = "http://hl7.org/fhir/StructureDefinition/elementdefinition-isCommonBinding"
* communication ^binding.extension[=].valueBoolean = true


Instance: PractitionerJulgeHunt
InstanceOf: EEBasePractitioner
Usage: #example
* id = "Pract1"
* identifier[medre].system = "urn:pin:hl7.ee:org:est:prn:70008799"
* identifier[medre].value = "D12345"
* active = true
* name.family = "Hunt"
* name.given = "Julge"
* address.line = "534 Erewhon St"
* address.city = "PleasantVille"
* address.postalCode = "3999"

Instance: PractitionerD05647
InstanceOf: EEBasePractitioner
Usage: #example
* id = "D05647"
* identifier[medre].system = "urn:pin:hl7.ee:org:est:prn:70008799"
* identifier[medre].value = "D05647"
* identifier[est-id].system = "urn:pin:hl7.ee:pid:est:ni"
* identifier[est-id].value = "XXXXXXXXXXX1"
* active = true
* name.family = "Arst"
* name.given = "Hea"
* qualification.code.text = "pediaatria"
* qualification.code = http://terminology.hl7.org/CodeSystem/v2-0360#MD "Doctor of Medicine"

Instance: PractitionerD04281
InstanceOf: EEBasePractitioner
Usage: #example
* id = "D04281"
* identifier[medre].system = "urn:pin:hl7.ee:org:est:prn:70008799"
* identifier[medre].value = "D04281"
* identifier[est-id].system = "urn:pin:hl7.ee:pid:est:ni"
* identifier[est-id].value = "XXXXXXXXXXX2"
* active = true
* name.family = "Arst"
* name.given = "Hea"

Instance: PractitionerN12770
InstanceOf: EEBasePractitioner
Usage: #example
* id = "N12770"
* identifier[medre].system = "urn:pin:hl7.ee:org:est:prn:70008799"
* identifier[medre].value = "N12770"
* identifier[est-id].system = "urn:pin:hl7.ee:pid:est:ni"
* identifier[est-id].value = "YYYYYY3"
* active = true
* name.family = "Õde"
* name.given = "Tubli"


Instance: PractitionerN13001
InstanceOf: EEBasePractitioner
Usage: #example
* id = "N13001"
* identifier[medre].system = "urn:pin:hl7.ee:org:est:prn:70008799"
* identifier[medre].value = "N13001"
* identifier[est-id].system = "urn:pin:hl7.ee:pid:est:ni"
* identifier[est-id].value = "YYYYYY4"
* active = true
* name.family = "Õde"
* name.given = "Tubli"

Instance: PractitionerN00778
InstanceOf: EEBasePractitioner
Usage: #example
* id = "N00778"
* identifier[medre].system = "urn:pin:hl7.ee:org:est:prn:70008799"
* identifier[medre].value = "N00778"
* identifier[est-id].system = "urn:pin:hl7.ee:pid:est:ni"
* identifier[est-id].value = "YYYYYY5"
* active = true
* name.family = "Õde"
* name.given = "Tubli"


Instance: PractitionerN14105
InstanceOf: EEBasePractitioner
Usage: #example
* id = "N14105"
* identifier[medre].system = "urn:pin:hl7.ee:org:est:prn:70008799"
* identifier[medre].value = "N14105"
* identifier[est-id].system = "urn:pin:hl7.ee:pid:est:ni"
* identifier[est-id].value = "YYYYYY6"
* active = true
* name.family = "Õde"
* name.given = "Tubli"

