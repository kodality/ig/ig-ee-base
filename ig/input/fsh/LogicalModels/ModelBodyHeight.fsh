Logical: BodyHeightModel
Parent: Element
Id: BodyHeightModel
* patient 1..1 Element "Patient" "Patsient" 
  * value 1..1 string "Patient identifier" "Isikukood"
  * system 0..1 string "Identity system" "Identifitseerimissüsteem"
* timestamp 0..1 dateTime "Timestamp of measurement" "Mõõtmiseaeg"
* value 1..1 decimal "Value (cm)" "Pikkus (cm)"

/*
Instance: ExampleBodyWeightModel
InstanceOf: TemperatureModel
Usage: #example
* patient 
  * value = "37302102711"
* timestamp = "2022-12-18"
* value = #36.6
*/
