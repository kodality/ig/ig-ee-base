Logical: BodyTemperatureModel
Parent: Element
Id: BodyTemperatureModel
* patient 1..1 Element "Patient" "Patsient" 
  * value 1..1 string "Patient identifier" "Isikukood"
  * system 0..1 string "Identity system" "Identifitseerimissüsteem"
* timestamp 0..1 dateTime "Timestamp of measurement" "Mõõtmiseaeg"
* value 1..1 decimal "Value (Cel)" "Väärtus (Cel)"

/*
Instance: ExampleBodyTemperatureModel
InstanceOf: BodyTemperatureModel
Usage: #example
* patient 
  * value = "37302102711"
* timestamp = "2022-12-18"
* value = #36.6
*/

/*
Mapping:  BodyTemperatureModelFHIRMapping
Source:   BodyTemperatureModel
Target:   "Observation"
Id:       hs-observation-temperature 
Title:    "Observation temperature mapping"
* -> "Observation"
* -> "category = http://terminology.hl7.org/CodeSystem/observation-category#vital-signs"
* -> "code = http://loinc.org#8310-5"
* patient -> "subject"
* timestamp -> "effectiveDateTime"
* value -> "valueQuantity.value"
* value -> "valueQuantity.unit = http://unitsofmeasure.org#Cel"
*/



